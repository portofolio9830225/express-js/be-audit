const multer = require("multer");

const DIR = "./public/tmp/";
const MIMETYPES = ["image/jpeg", "image/jpg", "image/png", "application/pdf"];

const storage = multer.diskStorage({
  destination: (req, file, callback) => {
    callback(null, DIR);
  },
  filename: (req, file, callback) => {
    const fileName = `${file.fieldname}_${Date.now()}_${file.originalname}`;
    callback(null, fileName);
  },
});

const fileFilter = (req, file, callback) => {
  if (MIMETYPES.includes(file.mimetype)) {
    callback(null, true);
  } else {
    callback(null, false);
    return callback(
      new Error("Only .png, .jpg, pdf and .jpeg format allowed!")
    );
  }
};

var uploadFile = multer({ storage, fileFilter });

module.exports = { uploadFile, DIR, MIMETYPES, fileFilter };
