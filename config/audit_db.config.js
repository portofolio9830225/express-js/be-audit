const mongoose = require("mongoose");
require("dotenv").config();
const debug = require("debug")("backend-audit:audit-db-config");

let dbConnection = null;
let dbOptions = null;

const GET_ENV = process.env.NODE_ENV;

if (["test", "development"].includes(GET_ENV.toLowerCase())) {
	//
	dbConnection = process.env.MONGO_CLOUD_TEST
		? `${process.env.MONGO_CLOUD_TEST}`
		: `mongodb://${process.env.MONGO_HOST_TEST}:${process.env.MONGO_PORT}/${process.env.MONGO_DB_TEST}`;

	//
	dbOptions = {
		useNewUrlParser: true,
		useUnifiedTopology: true,
		authSource: "admin",
		user: process.env.MONGO_USERNAME_TEST,
		pass: process.env.MONGO_PASSWORD_TEST,
	};
} else {
	//
	dbConnection = process.env.MONGO_CLOUD
		? `${process.env.MONGO_CLOUD}`
		: `mongodb://${process.env.MONGO_HOST}:${process.env.MONGO_PORT}/${process.env.MONGO_DB}`;

	//
	dbOptions = {
		useNewUrlParser: true,
		useUnifiedTopology: true,
		authSource: "admin",
		user: process.env.MONGO_USERNAME,
		pass: process.env.MONGO_PASSWORD,
	};
}

const connection = mongoose.createConnection(dbConnection, dbOptions);

connection.on("open", function (ref) {
	debug(`open connection to ${dbConnection}.`);
});

connection.on(`connected`, function (ref) {
	console.log(`connected to ${dbConnection}.`);
	debug(`connected to ${dbConnection}.`);
});

connection.on(`disconnected`, function (ref) {
	debug(`disconnected from ${dbConnection}.`);
});

connection.on(`close`, function (ref) {
	debug(`close connection to ${dbConnection}`);
});

connection.on(`error`, function (err) {
	debug(`error connection to ${dbConnection}!`);
	debug(`caused by : ${err}`);
});

connection.on(`reconnect`, function (ref) {
	debug(`reconnect to ${dbConnection}.`);
});
module.exports = connection;
