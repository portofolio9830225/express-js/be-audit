const express = require("express");
const router = express.Router();
const debug = require("debug")("backend-audit:file-routes");
const BackendValidator = require("../middlewares/BackendValidator");
const { DIR, upload } = require("../middlewares/FileValidator");
const { existsSync, mkdirSync, unlinkSync } = require("fs");
const path = require("path");

const fileModule = require("../modules/fileModule");

router.post("/", BackendValidator.isValidRequest, async (req, res) => {
  try {
    const x_app_token = req.header("x-application-token");

    // create a directory if it doesn't exist
    if (!existsSync(DIR)) {
      debug("CREATE DIRECTORY");
      mkdirSync(DIR, { recursive: true });
    }

    // upload file to local server
    upload(req, res, async (error) => {
      //
      if (error) {
        debug(error);
        debug("===> ERROR UPLOAD FILE TO BACKEND");
        return res.status(400).send("Upload Error! Contact Admin!");
      }

      //
      const payload = {
        department: req.body?.department,
        files: req.files,
        app_token: x_app_token,
      };

      //
      const result = await fileModule.upload(payload);

      //
      for (let f of payload.files) {
        // remove files when finish make call
        unlinkSync(path.join(__dirname, `/../${f.path}`));
      }

      if (result?.error) {
        return res.status(400).send(result);
      }

      return res.send(result);
    });
  } catch (err) {
    debug(err);
    return res.status(500).send(err);
  }
});

router.get(
  "/by-onedrive-id",
  BackendValidator.isValidRequest,
  async (req, res) => {
    try {
      const payload = req.query;

      const result = await fileModule.readByIdOneDrive(payload);

      if (result?.error) {
        return res.status(400).send(result);
      }

      return res.send(result);
    } catch (err) {
      debug(err);
      return res.status(500).send(err);
    }
  }
);

module.exports = router;
