const express = require("express");
const router = express.Router();
const debug = require("debug")("backend-audit:user-routes");
const BackendValidator = require("../middlewares/BackendValidator");
const teamModule = require("../modules/teamModule");

const userModule = require("../modules/userModule");

router.get("/all-department", BackendValidator.isValidRequest, async (req, res) => {
	try {
		const payload = req.query;
		payload.user_token = req.header("x-user-token");

		const result = await userModule.getAllDepartment(payload);

		if (result?.error) {
			return res.status(400).send(result);
		}

		return res.send(result);
	} catch (err) {
		debug(err);
		return res.status(500).send(err);
	}
});

router.get("/all-role", BackendValidator.isValidRequest, async (req, res) => {
	try {
		const payload = req.query;
		payload.user_token = req.header("x-user-token");

		const result = await userModule.getAllRole(payload);

		if (result?.error) {
			return res.status(400).send(result);
		}

		return res.send(result);
	} catch (err) {
		debug(err);
		return res.status(500).send(err);
	}
});

router.get("/by-dept-div-pos", BackendValidator.isValidRequest, async (req, res) => {
	try {
		const payload = req.query;
		payload.user_token = req.header("x-user-token");

		const result = await userModule.getUserBy(payload);

		if (result?.error) {
			return res.status(400).send(result);
		}

		return res.send(result);
	} catch (err) {
		debug(err);
		return res.status(500).send(err);
	}
});

router.get("/detail-by-id", BackendValidator.isValidRequest, async (req, res) => {
	try {
		const payload = req.query;
		payload.user_token = req.header("x-user-token");

		const result = await userModule.getUserById(payload);

		if (result?.error) {
			return res.status(400).send(result);
		}

		return res.send(result);
	} catch (err) {
		debug(err);
		return res.status(500).send(err);
	}
});

router.get("/upper-level", BackendValidator.isValidRequest, async (req, res) => {
	try {
		const payload = req.query;
		payload.user_token = req.header("x-user-token");

		const result = await userModule.getUpperLevel(payload);

		if (result?.error) {
			return res.status(400).send(result);
		}

		return res.send(result);
	} catch (err) {
		debug(err);
		return res.status(500).send(err);
	}
});

router.get("/under-level", BackendValidator.isValidRequest, async (req, res) => {
	try {
		const payload = req.query;
		payload.user_token = req.header("x-user-token");

		const result = await userModule.getUnderLevel(payload);

		if (result?.error) {
			return res.status(400).send(result);
		}

		return res.send(result);
	} catch (err) {
		debug(err);
		return res.status(500).send(err);
	}
});

router.post("/get-approval", BackendValidator.isValidRequest, async (req, res) => {
	try {
		const payload = req.body;
		payload.user_token = req.header("x-user-token");

		const result = await userModule.getApproval(payload);

		if (result?.error) {
			return res.status(400).send(result);
		}

		return res.send(result);
	} catch (err) {
		debug(err);
		return res.status(500).send(err);
	}
});

router.get("/upper-level-target", BackendValidator.isValidRequest, async (req, res) => {
	try {
		const payload = req.query;
		payload.user_token = req.header("x-user-token");

		const result = await userModule.getUpperLevelTarget(payload);

		if (result?.error) {
			return res.status(400).send(result);
		}

		return res.send(result);
	} catch (err) {
		debug(err);
		return res.status(500).send(err);
	}
});

router.get("/store-team", BackendValidator.isValidRequest, async (req, res) => {
	try {
		const payload = req.query;

		const result = await teamModule.getTeam(payload);
		return res.json(result);
	} catch (err) {
		return res.status(500).send(err);
	}
});

router.post("/store-team", BackendValidator.isValidRequest, async (req, res) => {
	try {
		const payload = req.body;
		const result = await teamModule.storeTeam(payload);
		return res.json(result);
	} catch (err) {
		return res.status(500).send(err);
	}
});

router.put("/update-team", BackendValidator.isValidRequest, async (req, res) => {
	try {
		const payload = req.body;
		const result = await teamModule.updateTeam(payload);
		return res.json(result);
	} catch (err) {
		return res.status(500).send(err);
	}
});

router.delete("/delete-team", BackendValidator.isValidRequest, async (req, res) => {
	try {
		const payload = req.query;

		const result = await teamModule.deleteTeam(payload);
		return res.json(result);
	} catch (err) {
		return res.status(500).send(err);
	}
});

module.exports = router;
