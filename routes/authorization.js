const express = require("express");
const router = express.Router();
const debug = require("debug")("backend-audit:authorization-routes");
const authorizationModule = require("../modules/authorizationModule");
const BackendValidator = require("../middlewares/BackendValidator");
const helper = require("../common/helper");

router.post(
  "/",
  [BackendValidator.isValidRequest, BackendValidator.checkDuplicateRole],
  async (req, res) => {
    try {
      const payload = req.body;

      const result = await authorizationModule.create(payload);

      if (result?.error) {
        return res.status(400).send(result);
      }

      return res.send(result);
    } catch (err) {
      debug(err);
      return res.status(500).send(err);
    }
  }
);

router.get("/", BackendValidator.isValidRequest, async (req, res) => {
  try {
    const payload = req.query;
    payload.showAll = ["true", "1"].includes(payload?.showAll?.toLowerCase());

    const result = await authorizationModule.read(payload);

    if (result?.error) {
      return res.status(400).send(result);
    }

    return res.send(result);
  } catch (err) {
    debug(err);
    return res.status(500).send(err);
  }
});

router.get("/:role", BackendValidator.isValidRequest, async (req, res) => {
  try {
    const payload = req.params;

    const result = await authorizationModule.readByRole(payload);

    if (result?.error) {
      return res.status(400).send(result);
    }

    return res.send(result);
  } catch (err) {
    debug(err);
    return res.status(500).send(err);
  }
});

router.put("/", BackendValidator.isValidRequest, async (req, res) => {
  try {
    const payload = req.body;

    const result = await authorizationModule.update(payload);

    if (result?.error) {
      return res.status(400).send(result);
    }

    return res.send(result);
  } catch (err) {
    debug(err);
    return res.status(500).send(err);
  }
});

router.delete("/", BackendValidator.isValidRequest, async (req, res) => {
  try {
    const payload = req.body;

    const result = await authorizationModule.destroy(payload);

    if (result?.error) {
      return res.status(400).send(result);
    }

    return res.send(result);
  } catch (err) {
    debug(err);
    return res.status(500).send(err);
  }
});
module.exports = router;
