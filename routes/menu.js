const express = require("express");
const router = express.Router();
const debug = require("debug")("backend-audit:menu-routes");
const menuModule = require("../modules/menuModule");
const BackendValidator = require("../middlewares/BackendValidator");
const helper = require("../common/helper");

router.post(
  "/",
  [BackendValidator.isValidRequest, BackendValidator.checkDuplicateNameLink],
  async (req, res) => {
    try {
      const payload = req.body;

      const result = await menuModule.create(payload);

      if (result?.error) {
        return res.status(400).send(result);
      }

      return res.send(result);
    } catch (err) {
      debug(err);
      return res.status(500).send(err);
    }
  }
);

router.get("/", BackendValidator.isValidRequest, async (req, res) => {
  try {
    const payload = req.body;

    const result = await menuModule.read(payload);

    if (result?.error) {
      return res.status(400).send(result);
    }

    return res.send(result);
  } catch (err) {
    debug(err);
    return res.status(500).send(err);
  }
});

router.get("/:id", BackendValidator.isValidRequest, async (req, res) => {
  try {
    const payload = req.body;
    let result = {};
    const { id } = req.params;

    if (!helper.isEmptyObject(id)) {
      if (id === "all") {
        result = await menuModule.readAllMenuSubmenu();
      } else {
        let payl = { _id: id };
        result = await menuModule.readById(payl);
      }
    }
    if (result?.error) {
      return res.status(400).send(result);
    }

    return res.send(result);
  } catch (err) {
    debug(err);
    return res.status(500).send(err);
  }
});

router.put("/", BackendValidator.isValidRequest, async (req, res) => {
  try {
    const payload = req.body;

    const result = await menuModule.update(payload);

    if (result?.error) {
      return res.status(400).send(result);
    }

    return res.send(result);
  } catch (err) {
    debug(err);
    return res.status(500).send(err);
  }
});

router.delete("/", BackendValidator.isValidRequest, async (req, res) => {
  try {
    const payload = req.body;

    const result = await menuModule.destroy(payload);

    if (result?.error) {
      return res.status(400).send(result);
    }

    return res.send(result);
  } catch (err) {
    debug(err);
    return res.status(500).send(err);
  }
});
module.exports = router;
