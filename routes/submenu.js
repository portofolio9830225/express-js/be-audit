const express = require("express");
const router = express.Router();
const debug = require("debug")("backend-audit:submenu-routes");
const submenuModule = require("../modules/submenuModule");
const BackendValidator = require("../middlewares/BackendValidator");
const helper = require("../common/helper");

router.get(
  "/submenu/:id",
  BackendValidator.isValidRequest,
  async (req, res) => {
    try {
      const payload = req.body;
      let result = {};

      if (!helper.isEmptyObject(req.params.id)) {
        let payl = { _id: req.params.id };
        result = await submenuModule.readByIdSubmenu(payl);
      }

      if (result?.error) {
        return res.status(400).send(result);
      }

      return res.send(result);
    } catch (err) {
      debug(err);
      return res.status(500).send(err);
    }
  }
);

router.get("/:id", BackendValidator.isValidRequest, async (req, res) => {
  try {
    const payload = { menu: req.params.id };
    debug(payload);
    let result = {};
    result = await submenuModule.readAllSubmenu(payload);

    if (result?.error) {
      return res.status(400).send(result);
    }

    return res.send(result);
  } catch (err) {
    debug(err);
    return res.status(500).send(err);
  }
});
router.post(
  "/",
  [
    BackendValidator.isValidRequest,
    BackendValidator.checkDuplicateNameLinkSubmenu,
  ],
  async (req, res) => {
    try {
      const payload = req.body;

      const result = await submenuModule.createSubmenu(payload);

      if (result?.error) {
        return res.status(400).send(result);
      }

      return res.send(result);
    } catch (err) {
      debug(err);
      return res.status(500).send(err);
    }
  }
);

router.put("/", BackendValidator.isValidRequest, async (req, res) => {
  try {
    const payload = req.body;

    const result = await submenuModule.updateSubmenu(payload);

    if (result?.error) {
      return res.status(400).send(result);
    }

    return res.send(result);
  } catch (err) {
    debug(err);
    return res.status(500).send(err);
  }
});

router.delete("/", BackendValidator.isValidRequest, async (req, res) => {
  try {
    const payload = req.body;

    const result = await submenuModule.deleteSubmenu(payload);

    if (result?.error) {
      return res.status(400).send(result);
    }

    return res.send(result);
  } catch (err) {
    debug(err);
    return res.status(500).send(err);
  }
});
module.exports = router;
