const express = require("express");
const router = express.Router();
const debug = require("debug")("backend-audit:alert-routes");
const BackendValidator = require("../middlewares/BackendValidator");

const alertModule = require("../modules/alertModule");

router.post("/send-mail", BackendValidator.isValidRequest, async (req, res) => {
  try {
    const payload = req.body;

    const result = await alertModule.sendMail(payload);

    if (result?.error) {
      return res.status(400).send(result);
    }

    return res.send(result);
  } catch (err) {
    debug(err);
    return res.status(500).send(err);
  }
});

module.exports = router;
