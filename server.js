require("dotenv").config();
const express = require("express");
const bodyParser = require("body-parser");
const path = require("path");
const cookieParser = require("cookie-parser");
const logger = require("morgan");
const fs = require("fs");
const helmet = require("helmet");
const debug = require("debug")("backend-audit:server");
const error = require("./common/errorMessage");
const rfs = require("rotating-file-stream");
const app = express();
const cors = require("cors");
const cron = require("node-cron");

app.use(cors());

if (process.env.NODE_ENV === "production") {
  const accessLogStream = rfs.createStream("server.log", {
    interval: "1d", // rotate daily
    path: path.join(__dirname, "log"),
  });
  app.use(logger("combined", { stream: accessLogStream }));
} else {
  app.use(logger("combined"));
}
app.use(helmet());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(bodyParser.json());

app.use(express.static(path.join(__dirname, "public")));

// mongodb config
global.audit_db = require("./config/audit_db.config");

const arrayAllowEnv = ["production", "staging", "development"];

if (arrayAllowEnv.includes(process.env.NODE_ENV)) {
  //cron
}

//checking health pod
app.get("/health", (req, res) => {
  res.send(`Apps is healthy`);
});

app.get("/", (req, res) => {
  res.send(`Welcome to Backend E-Permit`);
});

const routes = [
  // DEFAULT PATH
  { path: "/auth", router: require("./routes/auth") },
  { path: "/menu", router: require("./routes/menu") },
  { path: "/submenu", router: require("./routes/submenu") },
  { path: "/authorization", router: require("./routes/authorization") },
  { path: "/file", router: require("./routes/file") },
  { path: "/user", router: require("./routes/user") },
  { path: "/alert", router: require("./routes/alert") },
  // ADD END POINT HERE
];

routes.forEach(({ path, router }) => {
  app.use(`/api${path}`, router);
});

app.use((req, res, next) => {
  return res
    .status(404)
    .send(error.errorReturn({ message: "Invalid routes!" }));
});
module.exports = app;
