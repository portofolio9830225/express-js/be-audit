const Joi = require("joi");
const error = require("../common/errorMessage");
const debug = require("debug")("backend-audit:alert-module");
const jwt = require("jsonwebtoken");
const axios = require("axios");
const domain = process.env.URL;

const sendMailSchema = Joi.object({
  name: Joi.string().required(),
  category: Joi.string().required(),
  subject: Joi.string().required(),
  body: Joi.string().required(),
  send_to: Joi.array().required(),
  send_cc: Joi.array(),
  send_bcc: Joi.array(),
  chat_id: Joi.string(),
  body_type: Joi.string().required().valid("html", "text"),
  category: Joi.string().required(),
  sending_type: Joi.string()
    .required()
    .valid("now", "once", "daily", "weekly", "monthly"),
  sending_day: Joi.string()
    .required()
    .valid(
      "Sunday",
      "Monday",
      "Tuesday",
      "Wednesday",
      "Thursday",
      "Friday",
      "Saturday",
      ""
    ),
  sending_date: Joi.string().required(),
  sending_time: Joi.string().required(),
  periode_start: Joi.string().required(),
  periode_end: Joi.string().required(),
});

function validateSendMailSchema(schema) {
  return sendMailSchema.validate(schema);
}

// const payload = {
//   pemohon: "",
//   departemen: "",
//   versi: "",
//   izin_kerja: "",
//   perusahaan: "",
//   tanggal_mulai: "",
//   tanggal_selesai: "",
//   lokasi_kerja: "",
//   deskripsi: "",
// };
async function createBodyApproval(subject = "", content = "", payload = null) {
  return `<!DOCTYPE html>
  <html>
  
  <head>
    <meta name="viewport" content="width=device-width" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Email</title>
    <style>
      img {
        border: none;
        -ms-interpolation-mode: bicubic;
        max-width: 100%;
      }
  
      body {
        background-color: #f6f6f6;
        font-family: sans-serif;
        -webkit-font-smoothing: antialiased;
        font-size: 14px;
        line-height: 1.4;
        margin: 0;
        padding: 0;
        -ms-text-size-adjust: 100%;
        -webkit-text-size-adjust: 100%;
      }
  
      table {
        border-collapse: separate;
        mso-table-lspace: 0pt;
        mso-table-rspace: 0pt;
        width: 100%;
      }
  
      table td {
        font-family: sans-serif;
        font-size: 14px;
        vertical-align: top;
      }
  
      .body {
        background-color: #f6f6f6;
        width: 100%;
      }
  
      .container {
        display: block;
        margin: 0 auto !important;
        max-width: 580px;
        padding: 10px;
        width: 580px;
      }
  
      .content {
        box-sizing: border-box;
        display: block;
        margin: 0 auto;
        max-width: 580px;
        padding: 10px;
      }
  
      .main {
        background: #ffffff;
        border-radius: 3px;
        width: 100%;
      }
  
      .wrapper {
        box-sizing: border-box;
        padding: 20px;
      }
  
      .content-block {
        padding-bottom: 10px;
        padding-top: 10px;
      }
  
      .footer {
        clear: both;
        margin-top: 10px;
        text-align: center;
        width: 100%;
      }
  
      .footer td,
      .footer p,
      .footer span,
      .footer a {
        color: #999999;
        font-size: 12px;
        text-align: center;
      }
  
      h1,
      h2,
      h3,
      h4 {
        color: #000000;
        font-family: sans-serif;
        font-weight: 400;
        line-height: 1.4;
        margin: 0;
        margin-bottom: 30px;
      }
  
      h1 {
        font-size: 35px;
        font-weight: 300;
        text-align: center;
        text-transform: capitalize;
      }
  
      p,
      ul,
      ol {
        font-family: sans-serif;
        font-size: 14px;
        font-weight: normal;
        margin: 0;
        margin-bottom: 15px;
      }
  
      p li,
      ul li,
      ol li {
        list-style-position: inside;
        margin-left: 5px;
      }
  
      a {
        color: #3498db;
        text-decoration: underline;
      }
  
      .btn {
        box-sizing: border-box;
        width: 100%;
      }
  
      .btn>tbody>tr>td {
        padding-bottom: 15px;
      }
  
      .btn table {
        width: auto;
      }
  
      .btn table td {
        background-color: #ffffff;
        border-radius: 0px;
        text-align: left;
        color: #000000;
        padding-right: 2px;
      }
  
      .btn a {
        box-sizing: border-box;
        cursor: pointer;
        display: inline-block;
        font-size: 14px;
        margin: 0;
        padding: 1px 10px;
        text-decoration: none;
        text-transform: capitalize;
      }
  
      .btn-primary table td {}
  
      .btn-info {
        background-color: #009688 !important;
        color: #ffffff;
        text-decoration: none;
        padding-left: 5px;
        padding-right: 5px;
        padding-top: 2px;
        padding-bottom: 2px;
      }
  
      .btn-danger {
        background-color: #f44336 !important;
        color: #ffffff;
        padding: 2px 5px;
        text-decoration: none;
      }
  
      .last {
        margin-bottom: 0;
      }
  
      .first {
        margin-top: 0;
      }
  
      .align-center {
        text-align: center;
      }
  
      .align-right {
        text-align: right;
      }
  
      .align-left {
        text-align: left;
      }
  
      .clear {
        clear: both;
      }
  
      .mt0 {
        margin-top: 0;
      }
  
      .mb0 {
        margin-bottom: 0;
      }
  
      .preheader {
        color: transparent;
        display: none;
        height: 0;
        max-height: 0;
        max-width: 0;
        opacity: 0;
        overflow: hidden;
        mso-hide: all;
        visibility: hidden;
        width: 0;
      }
  
      .powered-by a {
        text-decoration: none;
      }
  
      hr {
        border: 0;
        border-bottom: 1px solid #f6f6f6;
        margin: 20px 0;
      }
  
      @media only screen and (max-width: 620px) {
        table[class="body"] h1 {
          font-size: 28px !important;
          margin-bottom: 10px !important;
        }
  
        table[class="body"] p,
        table[class="body"] ul,
        table[class="body"] ol,
        table[class="body"] td,
        table[class="body"] span,
        table[class="body"] a {
          font-size: 16px !important;
        }
  
        table[class="body"] .wrapper,
        table[class="body"] .article {
          padding: 10px !important;
        }
  
        table[class="body"] .content {
          padding: 0 !important;
        }
  
        table[class="body"] .container {
          padding: 0 !important;
          width: 100% !important;
        }
  
        table[class="body"] .main {
          border-left-width: 0 !important;
          border-radius: 0 !important;
          border-right-width: 0 !important;
        }
  
        table[class="body"] .btn table {
          width: 100% !important;
        }
  
        table[class="body"] .btn a {
          width: 100% !important;
        }
  
        table[class="body"] .img-responsive {
          height: auto !important;
          max-width: 100% !important;
          width: auto !important;
        }
      }
  
      /* ------------------------------------- PRESERVE THESE STYLES IN THE HEAD ------------------------------------- */
      @media all {
        .ExternalClass {
          width: 100%;
        }
  
        .ExternalClass,
        .ExternalClass p,
        .ExternalClass span,
        .ExternalClass font,
        .ExternalClass td,
        .ExternalClass div {
          line-height: 100%;
        }
  
        .apple-link a {
          color: inherit !important;
          font-family: inherit !important;
          font-size: inherit !important;
          font-weight: inherit !important;
          line-height: inherit !important;
          text-decoration: none !important;
        }
      }
    </style>
  </head>
  
  <body class="">
    <table border="0" cellpadding="0" cellspacing="0" class="body">
      <tr>
        <td>&nbsp;</td>
        <td class="container">
          <div class="content">
            <table class="main">
              <!-- START MAIN CONTENT AREA -->
              <tr>
                <td class="wrapper">
                  <table border="0" cellpadding="0" cellspacing="0">
                    <tr>
                      <td>
                        <p>${content}</p>
                      </td>
                    </tr>
                    <!-- START MAIN CONTENT AREA -->
                    <tr>
                      <td class="wrapper">
                        <table border="0" cellpadding="0" cellspacing="0">
                          <tbody>
                            <tr>
                              <th style="font-size: 14px; border: 1px solid #000">
                                Pemohon
                              </th>
                              <th style="font-size: 14px; border: 1px solid #000">
                                ${payload?.pemohon}
                              </th>
                            </tr>
                            <tr>
                              <th style="font-size: 14px; border: 1px solid #000">
                                Departemen
                              </th>
                              <th style="font-size: 14px; border: 1px solid #000">
                                ${payload?.departemen}
                              </th>
                            </tr>
                            <tr>
                              <th style="font-size: 14px; border: 1px solid #000">
                                Versi
                              </th>
                              <th style="font-size: 14px; border: 1px solid #000">
                                ${payload?.versi}
                              </th>
                            </tr>
                            <tr>
                              <th style="font-size: 14px; border: 1px solid #000">
                                Izin Kerja
                              </th>
                              <th style="font-size: 14px; border: 1px solid #000">
                                ${payload?.izin_kerja}
                              </th>
                            </tr>
                            <tr>
                              <th style="font-size: 14px; border: 1px solid #000">
                                Perusahaan
                              </th>
                              <th style="font-size: 14px; border: 1px solid #000">
                                ${payload?.perusahaan}
                              </th>
                            </tr>
                            <tr>
                              <th style="font-size: 14px; border: 1px solid #000">
                                Mulai
                              </th>
                              <th style="font-size: 14px; border: 1px solid #000">
                                ${payload?.tanggal_mulai}
                              </th>
                            </tr>
                            <tr>
                              <th style="font-size: 14px; border: 1px solid #000">
                                Selesai
                              </th>
                              <th style="font-size: 14px; border: 1px solid #000">
                                ${payload?.tanggal_selesai}
                              </th>
                            </tr>
                            <tr>
                              <th style="font-size: 14px; border: 1px solid #000">
                                Perpanjangan
                              </th>
                              <th style="font-size: 14px; border: 1px solid #000">
                                ${payload?.tanggal_perpanjangan}
                              </th>
                            </tr>
                            <tr>
                              <th style="font-size: 14px; border: 1px solid #000">
                                Catatan
                              </th>
                              <th style="font-size: 14px; border: 1px solid #000">
                                ${payload?.catatan}
                              </th>
                            </tr>
                            <tr>
                              <th style="font-size: 14px; border: 1px solid #000">
                                Lokasi Kerja
                              </th>
                              <th style="font-size: 14px; border: 1px solid #000">
                                ${payload?.lokasi_kerja}
                              </th>
                            </tr>
                            <tr>
                              <th style="font-size: 14px; border: 1px solid #000">
                                Deskripsi
                              </th>
                              <th style="font-size: 14px; border: 1px solid #000">
                                ${payload?.deskripsi}
                              </th>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
              <!-- END MAIN CONTENT AREA -->
            </table>
            <!-- START FOOTER -->
            <div class="footer">
              <table border="0" cellpadding="0" cellspacing="0">
                <tr>
                  <td class="content-block">
                    <span class="apple-link">PT. Lautan Natural Krimerindo</span>
                    <br />
                    view in Browser? <a href="${domain}/work-permit/approval">Click here</a>.
                  </td>
                </tr>
                <tr>
                  <td class="content-block powered-by">
                    Powered by <a href="#">MIS</a>.
                  </td>
                </tr>
              </table>
            </div>
            <!-- END FOOTER -->
            <!-- END CENTERED WHITE CONTAINER -->
          </div>
        </td>
        <td>&nbsp;</td>
      </tr>
    </table>
  </body>
  
  </html>`;
}

async function sendMail(payload) {
  try {
    // debug(payload);
    debug("===> PAYLOAD SEND MAIL");

    const validate = validateSendMailSchema(payload);
    if (validate.error) {
      return error.errorReturn({ message: validate.error.details[0].message });
    }

    const jwt_token = jwt.sign(
      { name: process.env.NAME, type: "private_apps" },
      process.env.PRIVATE_KEY,
      { expiresIn: "24h" }
    );
    const URL_ALERT_SERVICE = `${process.env.GATEWAY_IP}:${process.env.GATEWAY_PORT}/api/alert/`;

    const DATA = { ...payload, send_bcc: payload?.send_bcc };
    if (["production", "staging"].includes(process.env.NODE_ENV)) {
      DATA.send_bcc = [
        ...DATA.send_bcc,
        {
          name: process.env.BCC_NAME,
          address: process.env.BCC_ADDRESS,
        },
      ];
    }

    const result = await axios.post(URL_ALERT_SERVICE, DATA, {
      headers: {
        "x-application-token": `Bearer ${jwt_token}`,
        "x-public-key": `${process.env.PUBLIC_KEY}`,
      },
    });

    if (result?.error) {
      debug(result?.response?.data);
      debug("===> RESULT ERROR SEND MAIL");
      return result?.response?.data;
    }

    // debug(result.data);
    debug("===> RESULT SEND MAIL");

    return result.data;
  } catch (e) {
    debug(e);
    debug("===> ERROR SEND MAIL");
    throw error.errorReturn({ message: "Internal Server Error" });
  }
}

module.exports = { createBodyApproval, sendMail };
