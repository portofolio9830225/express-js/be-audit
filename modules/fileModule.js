const jwt = require("jsonwebtoken");
const Joi = require("joi");
const error = require("../common/errorMessage");
const debug = require("debug")("backend-audit:file-module");
const { db_audit } = require("../models");
const helper = require("../common/helper");
const File = db_audit.file;
const axios = require("axios");
const FormData = require("form-data");
const { readFileSync } = require("fs");
const PATH = require("path");

const uploadSchema = Joi.object({
  files: Joi.array().required(),
  app_token: Joi.string().required(),
  department: Joi.string().required(),
});

const convertHtmlToPDFSchema = Joi.object({
  html: Joi.string().required(),
  options: Joi.object().allow(null),
});

function validateUploadSchema(schema) {
  return uploadSchema.validate(schema);
}

function validateConvertHtmlToPDFSchema(schema) {
  return convertHtmlToPDFSchema.validate(schema);
}

async function upload(payload) {
  try {
    debug(payload);
    debug("===> PAYLOAD CREATE FILE");

    const validate = validateUploadSchema(payload);
    if (validate.error) {
      return error.errorReturn({ message: validate.error.details[0].message });
    }

    if (helper.isEmptyObject(payload.app_token)) {
      return error.errorReturn({ message: "Internal Server Error" });
    }

    if (payload.files.length > 0) {
      // init
      const END_POINT = "api/file/onedrive/upload-file/";
      const GATEWAY_IP = process.env.GATEWAY_IP;
      const GATEWAY_PORT = process.env.GATEWAY_PORT;
      const PUBLIC_KEY = process.env.PUBLIC_KEY;

      // init file
      const formData = new FormData();
      formData.append("apps", process.env.NAME);

      // load file
      payload.files.forEach((file) => {
        const readDir = PATH.join(__dirname, `/../${file.path}`);
        const readFile = readFileSync(readDir);
        formData.append("upload_file", readFile, file.path);
      });

      // init parameter
      const parameter = {
        method: "POST",
        url: `${GATEWAY_IP}:${GATEWAY_PORT}/${END_POINT}`,
        headers: {
          "x-application-token": `${payload.app_token}`,
          "x-public-key": PUBLIC_KEY,
          "Content-Type": `multipart/form-data;boundary=${formData.getBoundary()}`,
        },
        data: formData,
        maxContentLength: Infinity,
        maxBodyLength: Infinity,
      };
      //
      const result_upload_to_onedrive = await axios(parameter);

      if (result_upload_to_onedrive?.error) {
        debug(result_upload_to_onedrive);
        debug("===> RESULT UPLOAD TO ONEDRIVE");
        return error.errorReturn({ message: "Upload File to OneDrive Failed" });
      }

      debug(result_upload_to_onedrive?.data);
      debug("===> RESULT UPLOAD TO ONEDRIVE");

      return result_upload_to_onedrive.data;
    } else {
      return error.errorReturn({
        message: "File not found, can't be uploaded",
      });
    }
  } catch (e) {
    debug(e);
    debug("===> ERROR CREATE FILE");
    throw error.errorReturn({ message: "Internal Server Error" });
  }
}

async function readByIdOneDrive(query) {
  try {
    debug(query);
    debug("===> QUERY READ BY ID ONEDRIVE FILE");

    const jwt_token = jwt.sign(
      { name: process.env.NAME, type: "private_apps" },
      process.env.PRIVATE_KEY,
      { expiresIn: "2400h" }
    );

    const new_body = {};
    const new_query = { ...query };
    const new_url = "".concat(
      process.env.GATEWAY_IP,
      ":",
      process.env.GATEWAY_PORT,
      "/api/file/onedrive/read-file/"
    );

    const fileData = await axios.get(new_url, {
      headers: {
        "x-application-token": `Bearer ${jwt_token}`,
        "x-public-key": ` ${process.env.PUBLIC_KEY}`,
      },
      params: new_query,
    });

    if (fileData?.error) {
      return fileData;
    }

    return fileData?.data;
  } catch (e) {
    debug(e);
    debug("===> ERROR READ FILE");
    throw error.errorReturn({ message: "Internal Server Error" });
  }
}

async function convertHtmlToPDF(payload) {
  try {
    // debug(payload);
    debug("===> PAYLOAD HTML TO PDF");

    const validate = validateConvertHtmlToPDFSchema(payload);
    if (validate.error) {
      return error.errorReturn({ message: validate.error.details[0].message });
    }

    const jwt_token = jwt.sign(
      { name: process.env.NAME, type: "private_apps" },
      process.env.PRIVATE_KEY,
      { expiresIn: "2400h" }
    );

    const html_code = payload.html;

    const options = {
      border: {
        top: "0.1cm",
        right: "0.1cm",
        bottom: "0.1cm",
        left: "0.5cm",
      },
      height: "33.0cm",
      width: "22.0cm",
    };

    const data = payload?.options
      ? {
          options: JSON.stringify(payload.options),
          apps: process.env.NAME,
          html: html_code,
        }
      : {
          options: JSON.stringify(options),
          apps: process.env.NAME,
          html: html_code,
        };

    const new_body = { ...data };
    const new_query = {};
    const new_url = "".concat(
      process.env.GATEWAY_IP,
      ":",
      process.env.GATEWAY_PORT,
      "/api/file/convert/html-to-pdf/"
    );

    const fileData = await axios.post(new_url, new_body, {
      headers: {
        "x-application-token": `Bearer ${jwt_token}`,
        "x-public-key": ` ${process.env.PUBLIC_KEY}`,
        "Content-Type": "application/json",
      },
    });

    if (fileData?.error) {
      return fileData;
    }

    debug(fileData?.data);
    debug("===> RESULT HTML TO PDF");

    return fileData?.data;
  } catch (e) {
    debug(e);
    debug("===> ERROR HTML TO PDF");
    throw error.errorReturn({ message: "Internal Server Error" });
  }
}

module.exports = { upload, readByIdOneDrive, convertHtmlToPDF };
