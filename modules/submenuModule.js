const Joi = require("joi");
const error = require("../common/errorMessage");
const debug = require("debug")("backend-audit:submenu-module");
const helper = require("../common/helper");
const { db_audit } = require("../models");
const Submenu = db_audit.submenu;

const createSchema = Joi.object({
  requester: Joi.string().required(),
  name: Joi.string().required(),
  link: Joi.string().required(),
});

const updateSchema = Joi.object({
  _id: Joi.string().required(),
  requester: Joi.string().required(),
  name: Joi.string(),
  link: Joi.string(),
});

const deleteSchema = Joi.object({
  _id: Joi.string().required(),
  requester: Joi.string().required(),
});

const idSchema = Joi.object({
  _id: Joi.string().required(),
});

function validateCreateSchema(schema) {
  return createSchema.validate(schema);
}
function validateUpdateSchema(schema) {
  return updateSchema.validate(schema);
}
function validateDeleteSchema(schema) {
  return deleteSchema.validate(schema);
}
function validateIdSchema(schema) {
  return idSchema.validate(schema);
}

async function create(payload_params) {
  let payload = payload_params;
  const menuId = payload._id;
  delete payload._id;

  const validate = validateCreateSchema(payload);
  if (validate.error) {
    return error.errorReturn({ message: validate.error.details[0].message });
  }

  const requester = payload.requester;
  delete payload.requester;

  const new_values = { ...payload };

  new_values.is_active = true;
  new_values.link = `/${payload.link}`;
  new_values.created_at = new Date();
  new_values.created_by = payload.requester;
  new_values.menu = menuId;

  const submenu = await new Submenu(new_values).save();

  return submenu;
}

async function read(payload) {
  payload.is_active = true;
  const filter = payload;

  let submenu = await Submenu.find(filter).cursor();

  if (helper.isEmptyObject(submenu)) {
    return error.errorReturn({ message: "Submenu not found" });
  }

  let result = [];

  for (
    let _submenu = await submenu.next();
    _submenu != null;
    _submenu = await submenu.next()
  ) {
    let _submenu_result = {
      name: _submenu.name,
      link: _submenu.link.replace("/", ""),
      _id: _submenu._id,
    };
    result.push(_submenu_result);
  }

  return result;
}

async function update(payload) {
  const validate = validateUpdateSchema(payload);
  if (validate.error) {
    debug(validate.error);
    return error.errorReturn({ message: validate.error.details[0].message });
  }

  const filter = { _id: payload._id };
  const update = payload;
  update.update_at = new Date();
  update.update_by = payload.requester;

  if (helper.hasKey(payload, "link")) {
    update.link = `/${payload.link}`;
  }

  let submenu = await Submenu.findOneAndUpdate(filter, update, {
    useFindAndModify: false,
    new: true,
  });

  if (helper.isEmptyObject(submenu)) {
    return error.errorReturn({ message: "Submenu not found" });
  }

  return submenu;
}

async function destroy(payload) {
  const validate = validateDeleteSchema(payload);
  if (validate.error) {
    debug(validate.error);
    return error.errorReturn({ message: validate.error.details[0].message });
  }
  const filter = { _id: payload._id };

  const update = {
    updated_at: new Date(),
    updated_by: payload.requester,
    deleted_at: new Date(),
    deleted_by: payload.requester,
    is_active: false,
  };

  let submenu = await Submenu.findOneAndUpdate(filter, update, {
    useFindAndModify: false,
    new: true,
  });

  if (helper.isEmptyObject(submenu)) {
    return error.errorReturn({ message: "Submenu not found" });
  }

  return submenu;
}

async function readById(payload) {
  const validate = validateIdSchema(payload);
  if (validate.error) {
    debug(validate.error);
    return error.errorReturn({ message: validate.error.details[0].message });
  }

  const filter = { _id: payload._id };

  let submenu = await Submenu.findOne(filter);

  if (helper.isEmptyObject(submenu)) {
    return error.errorReturn({ message: "Submenu not found" });
  }

  return submenu;
}

module.exports = {
  create,
  read,
  update,
  destroy,
  readById,
};
