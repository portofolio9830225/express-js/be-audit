const { db_audit } = require("../models");
const Team_M = db_audit.team;

async function getTeam(param) {
	try {
		const sizePerPage = param.sizePerPage ? Number(param.sizePerPage) : 10;
		const page = param.page ? Number(query.page) : 1;
		const skip = sizePerPage * page - sizePerPage;

		const data = await Team_M.find({ deleted_at: null }).skip(skip).limit(sizePerPage);

		const numFound = await Team_M.find({ deleted_at: null });

		return {
			data: data,
			countData: Object.keys(numFound).length,
			currentPage: page,
			countPages: Math.ceil(Object.keys(numFound).length / sizePerPage),
		};
	} catch (err) {
		return "error";
	}
}

async function storeTeam(payload) {
	try {
		await Team_M.create({
			team_name: payload.team,
			team_code: payload.team,
			periode: payload.periode,
			member: payload.members,
		});
		return "sukses";
	} catch (err) {
		return "error";
	}
}

async function updateTeam(payload) {
	try {
		await Team_M.findByIdAndUpdate(payload.id, {
			team_name: payload.team,
			team_code: payload.team,
			periode: payload.periode,
			member: payload.members,
			updated_at: new Date(),
		});
		return "sukses";
	} catch (err) {
		return "error";
	}
}

async function deleteTeam(payload) {
	try {
		await Team_M.findByIdAndUpdate(payload.id, {
			deleted_at: new Date(),
		});
		return "sukses";
	} catch (err) {
		return "error";
	}
}

module.exports = {
	storeTeam,
	getTeam,
	deleteTeam,
	updateTeam,
};
