const Joi = require("joi");
const error = require("../common/errorMessage");
const debug = require("debug")("backend-audit:menu-module");
const helper = require("../common/helper");
const { db_audit } = require("../models");
const Menu = db_audit.menu;
const Submenu = db_audit.submenu;
const { ObjectId } = require("mongodb");

const createSchema = Joi.object({
  requester: Joi.string().required(),
  name: Joi.string().required(),
  icon: Joi.string().required(),
  link: Joi.string().required(),
});

const updateSchema = Joi.object({
  _id: Joi.string().required(),
  requester: Joi.string().required(),
  name: Joi.string(),
  icon: Joi.string(),
  link: Joi.string(),
});

const deleteSchema = Joi.object({
  _id: Joi.string().required(),
  requester: Joi.string().required(),
});

const idSchema = Joi.object({
  _id: Joi.string().required(),
  requester: Joi.string().required(),
});

function validateCreateSchema(schema) {
  return createSchema.validate(schema);
}
function validateUpdateSchema(schema) {
  return updateSchema.validate(schema);
}
function validateDeleteSchema(schema) {
  return deleteSchema.validate(schema);
}
function validateIdSchema(schema) {
  return idSchema.validate(schema);
}

async function create(payload_params) {
  let payload = payload_params;

  const validate = validateCreateSchema(payload);
  if (validate.error) {
    return error.errorReturn({ message: validate.error.details[0].message });
  }

  payload.is_active = true;
  payload.icon = `nav-icon  ${payload.icon}`;
  payload.link = `/${payload.link}`;
  payload.created_at = new Date();
  payload.created_by = payload.requester;

  const menu = await new Menu(payload).save();

  return { data: menu, status: "Data has been created successfully" };
}

async function read(query) {
  //query string
  const condition = { is_active: true };

  //sort
  const sortOrder = query?.sortOrder?.toLowerCase() == "asc" ? 1 : -1;
  const sortField = query?.sortBy ?? "date";
  const sort = { [sortField]: sortOrder };

  //skip
  const sizePerPage = query.sizePerPage ? Number(query.sizePerPage) : 100;
  const page = query.page ? Number(query.page) : 1;
  const skip = sizePerPage * page - sizePerPage;

  //limit
  const limit = sizePerPage;
  const menu = await Menu.find(condition)
    .sort(sort)
    .skip(skip)
    .limit(limit)
    .select("-__v -created_at -updated_at -is_active");

  const countDocuments = await Menu.countDocuments(condition);

  if (helper.isEmptyObject(menu)) {
    return {
      foundData: [],
      currentPage: page,
      countPages: 0,
      countData: 0,
    };
  }
  let result = [];

  menu.forEach((_menu) => {
    let _menu_result = {
      name: _menu.name,
      icon: _menu.icon.replace("nav-icon ", ""),
      link: _menu.link.replace("/", ""),
      _id: _menu._id,
    };
    result.push(_menu_result);
  });

  return {
    foundData: result,
    currentPage: page,
    countPages: Math.ceil(countDocuments / sizePerPage),
    countData: countDocuments,
  };
}

async function update(payload) {
  const validate = validateUpdateSchema(payload);
  if (validate.error) {
    debug(validate.error);
    return error.errorReturn({ message: validate.error.details[0].message });
  }
  const filter = { _id: ObjectId(payload._id) };
  let update = payload;

  if (helper.hasKey(payload, "link")) {
    update.link = `/${payload.link}`;
  }
  update.update_at = new Date();
  update.update_by = payload.requester;

  let menu = await Menu.findOneAndUpdate(filter, update, {
    useFindAndModify: false,
    new: true,
  });

  if (helper.isEmptyObject(menu)) {
    return error.errorReturn({ message: "Menu not found" });
  }

  return { data: menu, status: "Data has been updated successfully" };
}

async function destroy(payload) {
  const validate = validateDeleteSchema(payload);
  if (validate.error) {
    debug(validate.error);
    return error.errorReturn({ message: validate.error.details[0].message });
  }
  const filter = { _id: payload._id };
  const update = {
    updated_at: new Date(),
    updated_by: payload.requester,
    deleted_at: new Date(),
    deleted_by: payload.requester,
    is_active: false,
  };
  let menu = await Menu.findOneAndUpdate(filter, update, {
    useFindAndModify: false,
    new: true,
  });
  if (helper.isEmptyObject(menu)) {
    return error.errorReturn({ message: "Menu not found" });
  }

  return { data: menu, status: "Data has been deleted successfully" };
}

async function readById(payload) {
  const validate = validateIdSchema(payload);
  if (validate.error) {
    debug(validate.error);
    return error.errorReturn({ message: validate.error.details[0].message });
  }

  const filter = { _id: payload._id };

  let menu = await Menu.findOne(filter);

  if (helper.isEmptyObject(menu)) {
    return error.errorReturn({ message: "Menu not found" });
  }

  return menu;
}

async function readAllMenuSubmenu() {
  const menu = await Menu.find({ is_active: true }).cursor();
  const result = [];

  for (
    let _menu = await menu.next();
    _menu != null;
    _menu = await menu.next()
  ) {
    let _menu_result = {
      name: _menu.name,
      icon: _menu.icon,
      link: _menu.link,
    };
    let filter = { menu: _menu._id };
    let submenu = await Submenu.find(filter).cursor();
    let details = [];
    for (
      let _submenu = await submenu.next();
      _submenu != null;
      _submenu = await submenu.next()
    ) {
      let _submenu_result = {
        name: _submenu.name,
        link: _menu.link + _submenu.link,
      };
      details.push(_submenu_result);
    }
    _menu_result.details = details;
    result.push(_menu_result);
  }

  if (helper.isEmptyObject(result)) {
    return error.errorReturn({ message: "Menu not found" });
  }

  return result;
}

module.exports = {
  create,
  read,
  readById,
  update,
  destroy,
  readAllMenuSubmenu,
};
