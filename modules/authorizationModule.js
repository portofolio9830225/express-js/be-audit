const Joi = require("joi");
const error = require("../common/errorMessage");
const debug = require("debug")("backend-audit:authorization-module");
const helper = require("../common/helper");
const { db_audit } = require("../models");
const Authorization = db_audit.authorization;
const History = db_audit.history;
const { ObjectId } = require("mongodb");

const historyModule = require("./historyModule");

const createSchema = Joi.object({
  requester: Joi.string().required(),
  role: Joi.string().required(),
  menu: Joi.array().required(),
});

const updateSchema = Joi.object({
  _id: Joi.string().required(),
  requester: Joi.string().required(),
  role: Joi.string().required(),
  menu: Joi.array().required(),
});

const deleteSchema = Joi.object({
  _id: Joi.string().required(),
  requester: Joi.string().required(),
});

function validateCreateSchema(schema) {
  return createSchema.validate(schema);
}
function validateUpdateSchema(schema) {
  return updateSchema.validate(schema);
}
function validateDeleteSchema(schema) {
  return deleteSchema.validate(schema);
}

async function create(payload) {
  try {
    debug(payload);
    debug("===> PAYLOAD CREATE AUTHORIZATION");

    const validate = validateCreateSchema(payload);
    if (validate.error) {
      return error.errorReturn({ message: validate.error.details[0].message });
    }

    const find_exists = await Authorization.findOne({
      is_active: true,
      role: payload.role,
    }).lean();

    if (!helper.isEmptyObject(find_exists)) {
      return error.errorReturn({ message: "authorization already exists" });
    }

    const requester = payload.requester;
    delete payload.requester;

    const new_values = { ...payload };
    new_values.is_active = true;
    new_values.created_at = new Date();
    new_values.created_by = requester;

    const result = await new Authorization(new_values).save();

    debug(result);
    debug("===> RESULT CREATE AUTHORIZATION");

    return result;
  } catch (err) {
    debug(err);
    debug("===> ERROR CREATE AUTHORIZATION");
    throw error.errorReturn({ message: "Internal server error" });
  }
}

async function read(query) {
  try {
    debug(query);
    debug("===> PAYLOAD READ AUTHORIZATION");

    // query
    let filter = { is_active: true };

    // sort
    const sortOrder = query?.sortOrder?.toLowerCase() == "asc" ? 1 : -1;
    const sortField = query?.sortField ?? "created_at";
    const sort = { [sortField]: sortOrder };

    // skip
    const sizePerPage = query?.sizePerPage ? Number(query.sizePerPage) : 10;
    const page = query?.page ? Number(query.page) : 1;
    const skip = sizePerPage * page - sizePerPage;

    // limit
    const limit = sizePerPage;

    // find data
    let found_data = [];

    if (query?.showAll) {
      found_data = await Authorization.find(filter)
        .sort(sort)
        .collation({ locale: "en_US", numericOrdering: true })
        .select("-__v")
        .lean();
    } else {
      found_data = await Authorization.find(filter)
        .sort(sort)
        .collation({ locale: "en_US", numericOrdering: true })
        .skip(skip)
        .limit(limit)
        .select("-__v")
        .lean();
    }

    // if empty data
    if (helper.isEmptyArray(found_data)) {
      const result = {
        foundData: [],
        currentPage: page,
        countPages: 0,
        countData: 0,
      };
      debug(result);
      debug("===> RESULT READ AUTHORIZATION");
      return result;
    }

    //
    const count_data = await Authorization.countDocuments(filter);
    const current_page = query.showAll ? 1 : page;
    const count_page = query.showAll ? 1 : Math.ceil(count_data / sizePerPage);
    const result = {
      foundData: found_data,
      currentPage: current_page,
      countPages: count_page,
      countData: count_data,
    };

    debug(result);
    debug("===> RESULT READ AUTHORIZATION");

    return result;
  } catch (e) {
    debug(e);
    debug("===> ERROR READ AUTHORIZATION");
    throw error.errorReturn({ message: "Internal server error" });
  }
}

async function readByRole(query) {
  try {
    debug(query);
    debug("===> PAYLOAD READ BY ROLE AUTHORIZATION");

    if (helper.isEmptyObject(query.role)) {
      return error.errorReturn({ message: "role is empty" });
    }

    let filter = { is_active: true, role: query.role };
    let find = await Authorization.findOne(filter)
      .collation({ locale: "en_US", numericOrdering: true })
      .select("-__v")
      .lean();

    if (helper.isEmptyObject(find)) {
      filter = { role: "Guest User" };
      find = await Authorization.findOne(filter);
      return find.menu;
    }

    debug(find);
    debug("===> RESULT READ BY ROLE AUTHORIZATION");

    return find.menu;
  } catch (e) {
    debug(e);
    debug("===> ERROR READY BY ROLE AUTHORIZATION");
    throw error.errorReturn({ message: "Internal server error" });
  }
}

async function update(payload) {
  try {
    debug(payload);
    debug("===> PAYLOAD UPDATE AUTHORIZATION");

    const validate = validateUpdateSchema(payload);
    if (validate.error) {
      return error.errorReturn({ message: validate.error.details[0].message });
    }

    const id = payload._id;
    const requester = payload.requester;

    delete payload._id;
    delete payload.requester;

    const find_exists = await Authorization.findOne({
      _id: ObjectId(id),
      is_active: true,
    }).lean();

    if (helper.isEmptyObject(find_exists)) {
      return error.errorReturn({ message: "authorization not found" });
    }

    const old_data = await Authorization.findOne({ _id: ObjectId(id) }).lean();

    const new_data = { ...payload };
    new_data.updated_at = new Date();
    new_data.updated_by = requester;

    const result = await Authorization.findOneAndUpdate(
      { _id: ObjectId(id) },
      { ...new_data },
      { useFindAndModify: false, new: true }
    );

    debug(result);
    debug("===> RESULT UPDATE AUTHORIZATION");

    if (result?.error) {
      return error.errorReturn({ message: "Update failed" });
    }

    const new_history = {
      requester,
      id_history: result._id,
      collection_name: Authorization.collection.collectionName,
      data: old_data,
    };

    await historyModule.create(new_history);

    return result;
  } catch (err) {
    debug(err);
    debug("===> ERROR UPDATE AUTHORIZATION");
    throw error.errorReturn({ message: "Internal server error" });
  }
}

async function destroy(payload) {
  try {
    debug(data);
    debug("===> PAYLOAD DELETE AUTHORIZATION");

    const validate = validateDeleteSchema(payload);
    if (validate?.error) {
      return error.errorReturn({ message: validate.error.details[0].message });
    }

    const find_exists = await Authorization.findOne({
      _id: ObjectId(payload._id),
      is_active: true,
    }).lean();

    if (helper.isEmptyObject(find_exists)) {
      return error.errorReturn({ message: "authorization not found" });
    }

    const result = await Authorization.findOneAndUpdate(
      { _id: ObjectId(payload._id) },
      {
        is_active: false,
        updated_at: new Date(),
        updated_by: payload.requester,
        deleted_at: new Date(),
        deleted_by: payload.requester,
      },
      { useFindAndModify: false, new: true }
    );

    debug(result);
    debug("===> RESULT DELETE AUTHORIZATION");

    return result;
  } catch (e) {
    debug(e);
    debug("===> ERROR DELETE AUTHORIZATION");
    throw error.errorReturn({ message: "Internal Server Error" });
  }
}

module.exports = {
  create,
  read,
  readByRole,
  update,
  destroy,
};
