const mongoose = require("mongoose");
const audit_db = require("../config/audit_db.config");

const File = audit_db.model(
  "File",
  new mongoose.Schema({
    onedrive_id: { type: Object, required: true },
    originalname: { type: String, required: true },
    mimetype: { type: String, required: true },
    apps: { type: String, required: true },
    is_active: { type: String, required: true },
    created_at: { type: String },
    created_by: { type: String },
  }),
  "file"
);
module.exports = File;
