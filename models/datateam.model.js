const mongoose = require("mongoose");
const audit_db = require("../config/audit_db.config");

const DataTeam = audit_db.model(
	"data_team",
	new mongoose.Schema({
		team_name: { type: String, required: true },
		team_code: { type: String, required: true },
		periode: { type: String, required: true },
		member: { type: Array },
		is_active: { type: Boolean, default: true },
		created_at: { type: Date, default: Date.now },
		created_by: { type: String, default: null },
		updated_at: { type: Date, default: null },
		updated_by: { type: String, default: null },
		deleted_at: { type: Date, default: null },
		deleted_by: { type: String, default: null },
	}),
	"data_team"
);
module.exports = DataTeam;
