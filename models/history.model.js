const mongoose = require("mongoose");
const audit_db = require("../config/audit_db.config");

const History = audit_db.model(
  "History",
  new mongoose.Schema({
    id_history: { type: mongoose.Schema.Types.ObjectId, required: true },
    collection_name: { type: String, required: true },
    data: { type: Object, required: true },
    created_at: { type: Date, required: true },
    created_by: { type: String, required: true },
  }),
  "history"
);
module.exports = History;
