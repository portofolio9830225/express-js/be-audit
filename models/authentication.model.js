const mongoose = require("mongoose");
const audit_db = require("../config/audit_db.config");

const Authentication = audit_db.model(
  "Authentication",
  new mongoose.Schema({
    is_active: { type: Boolean },
    department: { type: String, required: true },
    username: { type: String, required: true },
    created_at: { type: Date },
    created_by: { type: String },
    updated_at: { type: Date },
    updated_by: { type: String },
    deleted_at: { type: Date },
    deleted_by: { type: String },
  }),
  "authentications"
);
module.exports = Authentication;
