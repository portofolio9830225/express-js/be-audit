const audit_db = require("../config/audit_db.config");
audit_db.Promise = global.Promise;

const db_audit = {};
db_audit.mongoose = audit_db;

// system
db_audit.authentication = require("./authentication.model");
db_audit.authorization = require("./authorization.model");
db_audit.file = require("./file.model");
db_audit.menu = require("./menu.model");
db_audit.submenu = require("./submenu.model");
db_audit.history = require("./history.model");
db_audit.team = require("./datateam.model");

module.exports = { db_audit };
