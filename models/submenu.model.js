const mongoose = require("mongoose");
const audit_db = require("../config/audit_db.config");

const Submenu = audit_db.model(
  "Submenu",
  new mongoose.Schema({
    name: { type: String, required: true },
    link: { type: String, required: true },
    is_active: { type: Boolean },
    created_at: { type: Date },
    created_by: { type: String },
    updated_at: { type: Date },
    updated_by: { type: String },
    deleted_at: { type: Date },
    deleted_by: { type: String },
    menu: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Menu",
    },
  }),
  "submenus"
);
module.exports = Submenu;
