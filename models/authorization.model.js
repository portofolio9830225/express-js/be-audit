const mongoose = require("mongoose");
const audit_db = require("../config/audit_db.config");

const Authorization = audit_db.model(
  "Authorization",
  new mongoose.Schema({
    role: { type: String, required: true },
    menu: { type: Object, required: true },
    menu_mobile: { type: Array },
    is_active: { type: Boolean },
    created_at: { type: Date },
    created_by: { type: String },
    updated_at: { type: Date },
    updated_by: { type: String },
    deleted_at: { type: Date },
    deleted_by: { type: String },
  }),
  "authorizations"
);
module.exports = Authorization;
