const mongoose = require("mongoose");
const audit_db = require("../config/audit_db.config");

const Menu = audit_db.model(
  "Menu",
  new mongoose.Schema({
    name: { type: String, required: true },
    icon: { type: String, required: true },
    link: { type: String, required: true },
    is_active: { type: Boolean },
    created_at: { type: Date },
    created_by: { type: String },
    updated_at: { type: Date },
    updated_by: { type: String },
    deleted_at: { type: Date },
    deleted_by: { type: String },
  }),
  "menus"
);
module.exports = Menu;
