function detectEncode(x) {
  // ie ?,=,&,/ etc
  return decodeURI(x) !== decodeURIComponent(x);
}
module.exports = {
  detectEncode,
};
